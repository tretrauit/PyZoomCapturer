import getpass
import discord # It's archived but still usable (and we don't use their dumb minecraft command system)
import info
import platform
import colorlog
import os
#from colorama import init, deinit
from PIL import ImageGrab
from datetime import datetime
from pathlib import Path
from cfgparser import CfgParser

# Apparently init breaks color in Windows right now.
#init()
logger = colorlog.logger
if info.DEBUG:
    colorlog.set_level("debug")
    logger.debug("This version has enabled debug logging.")

keyboard_backend = None
# Linux workaround for nonroot
if platform.system() == "Linux":
    if os.getuid() == 0: # If we're root
        if os.environ.get("SUDO_USER") != "" and getpass.getuser() == "root": # If we're sudo
            logger.warning("Root mode detected, please be careful when running with root.")
        else:
            # User executed using doas/su command but both doesn't pass SUDO_USER variable.
            # And running in root is not required unless you want to use keyboard module 
            # for better keybinding (and Wayland (not Xwayland) support)
            logger.critical("Do not run this app using superuser, if you need to use root then use sudo.")
            raise RuntimeError("Pure superuser detected, rejecting to run.")
    else:
        # User is not root so we can't use keyboard module, falling to pynput
        logger.warning("Using pynput backend, will not support Wayland")
        from pynput import keyboard
        keyboard_backend = "pynput"
    session_type = os.environ.get("XDG_SESSION_TYPE")
    if session_type == "wayland":
        logger.warning("Wayland detected, this app hasn't been tested with Wayland yet, \
and Zoom's included Qt is known to work better in XWayland. Use at your own risk.")

if keyboard_backend == None:
    # Import keyboard module in case we haven't imported any module yet
    logger.info("Using keyboard backend.")
    keyboard_backend = "keyboard"
    import keyboard

import functions

### Functions ###
class Main():

    # This is the event that triggers when you press the hotkey.
    def capture_event(self, *args, **kwargs):

        # Get Zoom window if it's in foreground.
        screenshot = functions.capture_screenshot(self.zoom_config["meeting_window_name"], self.zoom_config["foreground_check"])
        if screenshot is not None:

            # Use ImageGrab to get the screenshot.
            cutout = self.zoom_config["cutout"]
            if cutout["enabled"]:
                window_cut = cutout["window_cut"]
                w, h = screenshot.size
                screenshot = screenshot.crop((0 + window_cut[3], 0 + window_cut[0], w - window_cut[2], h - window_cut[1]))
                logger.debug("Cropped Zoom screenshot.")
            logger.debug("Successfully captured Zoom Meeting window.")

            # Generate screenshot name
            save_locations = self.config['save_locations']
            save_file_cfg = save_locations["file"]
            screenshot_name = f"{datetime.now().strftime(save_file_cfg['filename'])}.{save_file_cfg['file_format']}"
            logger.debug(f"Screenshot name: {screenshot_name}")
            notify_text = ""

            # Save the screenshot
            if save_file_cfg["enabled"]: 
                # Save to file.
                logger.debug("Save image is enabled, saving...")
                save_path = Path(save_file_cfg["location"])
                screenshot.save(save_path.joinpath(screenshot_name))
                logger.info(f"Saved to: {screenshot_name}")
                notify_text = screenshot_name
                
            if save_locations["clipboard"]: 
                # Save to clipboard.
                logger.debug("Copy to clipboard is enabled, copying...")
                functions.save_to_clipboard(screenshot)
                logger.info("Copied to clipboard")
                if len(notify_text) > 0:
                    notify_text += ", "
                notify_text += "Clipboard"

            discord_upload_cfg = save_locations["discord"]
            if discord_upload_cfg["enabled"] and discord_upload_cfg["webhook_uri"].strip(): 
                # Upload to Discord if the url is "valid" (poorly checked right now)
                logger.debug("Discord Webhook is enabled, uploading...")
                try:
                    functions.upload_to_discord(discord_upload_cfg["webhook_uri"], discord_upload_cfg["message"], screenshot)
                except (discord.errors.HTTPException, discord.errors.NotFound, discord.errors.Forbidden) as e:
                    logger.error(f"An error occured while sending the image: {e.message}")
                else:
                    logger.info("Image sent to Discord.")
                    if len(notify_text) > 0:
                        notify_text += "and "
                    notify_text += "Discord"
            
            matrix_upload_cfg = save_locations["matrix"]
            if matrix_upload_cfg["enabled"] and matrix_upload_cfg["room_id"].strip():
                # Upload to Matrix room (soon)
                matrix_error = NotImplementedError("Saving to Matrix isn't implemented yet.")
                self.logger.warning(matrix_error)
                
            notify_text = "Saved to " + notify_text
            # Show the notification to user.
            if save_locations["notification"]:
                functions.show_notification(notify_text)

    def _pynput_parse_keys_str(self, keys):
        # Parse keyboard module key combination to pynput type.
        # This is a workaround for pynput's limitation of only supporting <key> type.
        parsed_key = ""
        for key in keys.split('+'):
            logger.debug(f"Trying to parse {key}")
            try:
                # I'm trying to get a key here
                getattr(keyboard.Key, key)
            except AttributeError as e:
                logger.debug(f"Not a special key: {e}")
                parsed_key += key
            else:
                parsed_key += f"<{key}>"
            finally:
                parsed_key += "+"

        return parsed_key[:-1]

    def __init__(self):

        logger.info(f"PyZoomCapturer - v{info.VERSION} ({info.CODENAME})")
        logger.warning("This version should not be used in production.")

        self.cfgutil = CfgParser()
        self.config = self.cfgutil.config
        self.zoom_config = self.config["zoom"]

        logger.info("PyZoomCapturer started successfully.")
        logger.info(f"Keybind: {self.config['keybind']['screenshot']}")

        if self.config['save_locations']['file']['enabled']:
            if self.config['save_locations']['file']['location'].strip().lower() == "default":
                screenshot_location = Path.home().joinpath('./Pictures/PyZoomCapturer/')
                self.config['save_locations']['file']['location'] = str(screenshot_location)
                self.cfgutil.write()
            Path(self.config['save_locations']['file']['location']).mkdir(parents=True, exist_ok=True)
            logger.info(f"Save location: {self.config['save_locations']['file']['location']}")

        logger.info("If Zoom Meeting window isn't detected, change \
the config.json file to the correct localized name in the meeting window title.")

        if keyboard_backend == "keyboard": # Check if we use keyboard module (Usually on Windows/macOS and root Linux)
            keyboard.add_hotkey(self.config['keybind']['screenshot'], self.capture_event)
            try:
                keyboard.wait()
            except KeyboardInterrupt:
                pass
            except Exception as e:
                logger.exception("An error occured while the app is running.", e)

        elif keyboard_backend == "pynput":
            parsed_key = self._pynput_parse_keys_str(self.config['keybind']['screenshot'])
            logger.debug(f"Parsed key: {parsed_key}")

            def on_activate():
                self.capture_event()

            def for_canonical(f):
                return lambda k: f(listener.canonical(k))

            hotkey = keyboard.HotKey(
                keyboard.HotKey.parse(parsed_key),
                on_activate)
            with keyboard.Listener(on_press=for_canonical(hotkey.press), 
                on_release=for_canonical(hotkey.release)) as listener:
                try:
                    listener.join()
                except:
                    # Temporary exit because I forgot how to deinit pynput.
                    listener.stop()
                    
        logger.info("Exiting...")
        # We haven't init colorama yet.
        #deinit()
# Main entry point
if __name__ == "__main__":
    Main()