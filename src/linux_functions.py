import subprocess
import shutil
import os
import getpass
import colorlog as colorlog
import gi
import tempfile
gi.require_version('Wnck', '3.0')
gi.require_version("Gtk", "3.0")
from io import BytesIO
from pathlib import Path
from gi.repository import Wnck, Gtk, Gdk, GdkPixbuf
from PIL import ImageGrab

class linux_functions():
    # Dummy implementation
    def __init__(self):
        # This script path
        self.logger = colorlog.logger
        self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        self.path = Path(__file__).resolve().parent
        self.logger.warning("Linux implementation is not available, most functions are dummy.")
        self.workaround_args = None
        self._wnck_screen = Wnck.Screen.get_default()
        if getpass.getuser() == "root":
            self.logger.warning("Executing workaround for sudo root...")
            uid = subprocess.run(["id", "-u", os.environ["SUDO_USER"]], stdout=subprocess.PIPE).stdout.decode("utf-8").strip()
            self.logger.debug(f"User UID for sudo: {uid}")
            self.workaround_args = ['sudo', '-u', os.environ['SUDO_USER'], f'DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/{uid}/bus']
        self.notify_backend_excl = []

    # Detect Zoom window using Wnck
    def capture_screenshot(self, window_name: str, foreground_check: bool = True):
        # Fixme
        self.logger.debug("Foreground check is not implemented in Linux due to lack of documentation on how to do it.")
        self._wnck_screen.force_update()
        window = self._wnck_screen.get_active_window()
        if window is None:
            return None
        if window.get_icon_name() != window_name:
            self.logger.debug(f"Active window name: {window.get_icon_name()}")
            self.logger.debug(f"Window name: {window_name}")
            self.logger.debug("Active window is not the expected one, aborting...")
            return None
        geometry = window.get_client_window_geometry()
        self.logger.debug(f"Window geometry: {geometry}")
        return ImageGrab.grab(bbox=(geometry.xp, geometry.yp, geometry.widthp + geometry.xp, geometry.heightp + geometry.yp))

    def set_clipboard(self, content):
        # Warning message so the user will notice it.
        self.logger.warning("Clipboard feature in Linux has been disabled for stablity")
        return
        # I know these code is not going to be executed.
        with tempfile.NamedTemporaryFile(mode="w+b", delete=True, suffix='.png') as f:
            content.save(f, "png")
            image = GdkPixbuf.Pixbuf.new_from_file(f.name)
            self.clipboard.set_image(image)

    def show_notification(self, content):
        if shutil.which("dunstify") and not "dunstify" in self.notify_backend_excl:
            args = ["dunstify", "-a", "PyZoomCapturer", "-i", "logo.png", content]
            if self.workaround_args:
                args = self.workaround_args + args
            self.logger.debug(f"Arguments: {args}")
            result = None
            try:
                result = subprocess.run(args, cwd=self.path, timeout=5)
            except subprocess.TimeoutExpired:
                self.logger.error("Timeout occurred while using dunstify backend.")
            if isinstance(result, subprocess.CompletedProcess) and result.returncode == 0:
                return
            self.notify_backend_excl.append("dunstify")
            self.logger.error("Failed to use dunstify backend.")
            self.logger.debug("Blacklisted dunstify backend for current session.")

        if shutil.which("notify-send") and not "notify-send" in self.notify_backend_excl:
            args = ["notify-send", "-a", "PyZoomCapturer", "-i", "logo.png", content]
            if self.workaround_args:
                args = self.workaround_args + args
            self.logger.debug(f"Arguments: {args}")
            result = None
            try:
                result = subprocess.run(args, cwd=self.path, timeout=5)
            except subprocess.TimeoutExpired:
                self.logger.error("Timeout occurred while using notify-send backend.")
            if isinstance(result, subprocess.CompletedProcess) and result.returncode == 0:
                return
            self.notify_backend_excl.append("notify-send")
            self.logger.error("Failed to use notify-send backend.")
            self.logger.debug("Blacklisted notify-send backend for current session.")

        raise NotImplementedError("No notification backend are usable on your\
 system, supported implementation are 'dunstify' and 'notify-send'")
    