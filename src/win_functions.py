import win32gui
import win32clipboard
from winrt.windows.ui.notifications import ToastNotificationManager, ToastNotification, ToastTemplateType
import sys
from PIL import ImageGrab

class win_functions():

    # Detect Zoom window using Win32 API
    def capture_screenshot(self, window_name: str, foreground_check: bool = True):
        zoom_hwnd = win32gui.FindWindowEx(None, None, None, window_name)
        # If Zoom window is found and in foreground.
        if zoom_hwnd != 0 and (not foreground_check or zoom_hwnd == win32gui.GetForegroundWindow()):
            return ImageGrab.grab(bbox=win32gui.GetWindowRect(zoom_hwnd))
        else:
            return None

    # Set clipboard from bytes content
    def set_clipboard(self, content: bytes):
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardData(win32clipboard.CF_DIB, content)
        win32clipboard.CloseClipboard()

    def show_notification(self, content):
        XML = ToastNotificationManager.get_template_content(ToastTemplateType.TOAST_TEXT02)
        t = XML.get_elements_by_tag_name("text")
        t[0].append_child(XML.create_text_node("PyZoomCapturer"))
        t[1].append_child(XML.create_text_node(content))
        notifier = ToastNotificationManager.create_toast_notifier(sys.executable)
        notifier.show(ToastNotification(XML))
