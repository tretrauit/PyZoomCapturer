import logging
from pathlib import Path
from colorama import Fore, Style
import datetime

class CustomFormatter(logging.Formatter):
    format = "%(asctime)s [%(levelname)s]: %(message)s"

    FORMATS = {
        logging.DEBUG: Fore.WHITE + Style.DIM + format + Style.RESET_ALL,
        logging.INFO: Fore.CYAN + format + Style.RESET_ALL,
        logging.WARNING: Fore.YELLOW + format + Style.RESET_ALL,
        logging.ERROR: Fore.RED + Style.DIM +format + Style.RESET_ALL,
        logging.CRITICAL: Fore.RED + format + Style.RESET_ALL
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

log_path = Path("./log/")
log_path.mkdir(parents=True, exist_ok=True)
log_file = log_path.joinpath(datetime.datetime.now().strftime("%Y%m%d-%H%M%S") + ".log")
log_file.touch(exist_ok=True)

fh = logging.FileHandler(log_file)
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)
# Create console handler with a higher log level
con_handler = logging.StreamHandler()
con_handler.setLevel(logging.INFO)
con_handler.setFormatter(CustomFormatter())
logger.addHandler(con_handler)

def set_level(level):
    level = level.lower()
    if level == "notset":
        con_handler.setLevel(logging.NOTSET)
    elif level == "debug":
        con_handler.setLevel(logging.DEBUG)
    elif level == "info":
        con_handler.setLevel(logging.INFO)
    elif level == "warning":
        con_handler.setLevel(logging.WARNING)
    elif level == "error":
        con_handler.setLevel(logging.ERROR)
    elif level == "critical":
        con_handler.setLevel(logging.CRITICAL)
    else:
        raise ValueError("Invalid value level: %s" % level)