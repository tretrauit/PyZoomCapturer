import json
from colorlog import logger
from pathlib import Path

class CfgParser():
	def __init__(self, file = "./config.json"):
		self.file = Path(file)
		if not self.file.exists():
			print("Config file does not exist, creating a new one...")
			self.config = self.get_default_config()
			self.write()
		else:
			try:
				self.config = json.loads(self.safe_read())
			except json.decoder.JSONDecodeError:
				self.config = self.get_default_config()
				self.write()

	def safe_read(self):
		text = None
		with self.file.open("r+", encoding="utf-8") as f: # Force UTF-8 (damn ASCII Windows)
			text = f.read()
		logger.debug(f"Configuration: {text}")
		return text

	def safe_write(self, text):
		with self.file.open("w+") as f:
			f.write(text)

	def get_default_config(self):
		return {
			"zoom": {
				"meeting_window_name": "Zoom Meeting",
				"foreground_check": True,
				"cutout": {
					"enabled": True,
					"window_cut": [125, 0, 0, 0], # Top right bottom left
				}
			},
			"save_locations": {
				"clipboard": False,
				"file": {
					"enabled": True,
					"file_format": "png",
					"location": "default", # Default is User home folder/Pictures/PyZoomCapturer
					"filename": "%Y-%m-%d-%H-%M-%S"
				},
				"discord": {
					"enabled": False,
					"author": "<author>",
					"message": "",
					"webhook_uri": ""
				},
				"matrix": { # Matrix currently not implemented btw.
					"enabled": False,
					"username": "<username>",
					"password": "",
					"homeserver": "",
					"room_id": "",
					"cached": {
						"device_id": "",
						"access_token": "",
					}
				},
				"notification": True
			},
			"keybind": {
				"screenshot": "home"
			}
		}

	def reload(self, new_config = None):
		if new_config:
			self.file = Path(new_config)
			if not self.file.exists():
				raise FileNotFoundError("The config file does not exist")
	
		self.config = json.loads(self.safe_read())

	def write(self, new_config = None, new_file = None):
		if new_config:
			self.config = new_config

		if new_file:
			self.file = Path(new_config)
			
		self.safe_write(json.dumps(self.config, indent=4))
