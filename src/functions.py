import platform
import aiohttp
import asyncio
import discord
# from colorlog import logger
from PIL import Image
from io import BytesIO

os_functions = None
if platform.system() == 'Windows':
    from win_functions import win_functions
    os_functions = win_functions()
elif platform.system() == 'Linux':
    from linux_functions import linux_functions
    os_functions = linux_functions()
else:
    raise NotImplementedError("Operating system not supported.")

# Find the Zoom window
def capture_screenshot(window_name, foreground_check: bool = True):
    return os_functions.capture_screenshot(window_name, foreground_check)
# Utility for converting image to bytes object
def convert_image_to_bytes(image):
    image_bytes = BytesIO()
    image.convert("RGB").save(image_bytes, "bmp")
    return image_bytes.getvalue()[14:]

# Set the current clipboard
def save_to_clipboard(image):
    if platform.system() == 'Windows':
        os_functions.set_clipboard(convert_image_to_bytes(image))
    else:
        os_functions.set_clipboard(image)

# Show the notification 
def show_notification(message):
    os_functions.show_notification(message)

# Upload the screenshot to Discord using Webhook
async def upload_to_discord_async(webhook_url: str, message: str, content, filename: str = "unknown.png", username: str = "PyZoomCapturer",
avatar_url: str = "https://cdn.discordapp.com/attachments/801848004072046662/839546890836377691/89596288_p2.png"):
    if isinstance(content, Image.Image):
        content = BytesIO(convert_image_to_bytes(content))
    async with aiohttp.ClientSession() as session:
        webhook = discord.Webhook.from_url(webhook_url, adapter=discord.AsyncWebhookAdapter(session))
        await webhook.send(content=message, username=username, avatar_url=avatar_url, file=discord.File(content, filename=filename))

# Upload to Discord but include handling the async.
def upload_to_discord(webhook_url: str, message: str, content, filename: str = "unknown.png", username: str = "PyZoomCapturer",
avatar_url: str = "https://cdn.discordapp.com/attachments/801848004072046662/839546890836377691/89596288_p2.png"):
    asyncio.run(upload_to_discord_async(webhook_url, message, content, filename, username, avatar_url))
