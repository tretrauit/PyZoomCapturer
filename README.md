# PyZoomCapturer

A fast, unreliable tool for screenshotting Zoom Meeting.

### Features
+ Windows only (Linux soon)
+ Capture Zoom Meeting window using Pillow + Win32 API
+ Save screenshot to file, clipboard and Discord (through Webhooks)
+ Configurable through editing config.json
+ Bloated and unorganized codebase

### Install
+ Download from [releases](releases) or GitLab CI (soon)
+ Note: These build have [UPX](https://upx.github.io/) enabled so the size is significantly reduced.

### Building
- Automated building script coming soon, for now follow below instructions
- Run this in PowerShell:
```powershell
git clone https://gitlab.com/tretrauit/PyZoomCapturer.git
cd ./PyZoomCapturer
py -m venv venv
venv\Scripts\activate
pip install -r requirements.txt
pip install pyinstaller
pyinstaller PyZoomCapturer.spec
```
This should build the program and put it into `{project_root}/dist` directory

### License
[MIT License](LICENSE)