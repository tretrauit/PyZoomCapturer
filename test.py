#!/usr/bin/python3

print("Running test.py")
import sys
import argparse
sys.path.append('./src/')
import colorlog
import functions
import unittest
from cfgparser import CfgParser
from PIL import Image
print("Import completed.")

no_discord = False
class Test(unittest.TestCase):

    def test_logger(self):
        colorlog.set_level("debug")
        logger = colorlog.logger    
        self.assertIsNone(logger.debug("Debug from colorlog"), "Should be None for colorlog.logger.debug")
        self.assertIsNone(logger.info("Info from colorlog"), "Should be None for colorlog.logger.info")
        self.assertIsNone(logger.warning("Warning from colorlog"), "Should be None for colorlog.logger.warning")
        self.assertIsNone(logger.error("Error from colorlog"), "Should be None for colorlog.logger.error")
        self.assertIsNone(logger.exception("Exception from colorlog", RuntimeError("Debug exception")), "Should be None for colorlog.logger.exception")
        self.assertIsNone(logger.error("Critical from colorlog"), "Should be None for colorlog.logger.critical")

    def test_cfgparser(self):
        cfgutil = CfgParser()
        config = cfgutil.config
        bak_cfg = config
        self.assertTrue(isinstance(config, dict), "Should be True for cfgparser.config")
        config["keybind"]["screenshot"] = "delete"
        config = cfgutil.get_default_config()
        self.assertTrue(isinstance(config, dict), "Should be True after cfgparser.get_default_config")
        config = bak_cfg # Restore config
        self.assertIsNone(cfgutil.write(), "Should be None for cfgparser.write")

    def test_convert_image_to_bytes(self):
        test_image = Image.new('RGB', (1, 1))
        self.assertTrue(isinstance(functions.convert_image_to_bytes(test_image), bytes), "Should be True for isinstance(functions.convert_image_to_bytes, bytes)")

    def test_save_to_clipboard(self):
        test_image = Image.new('RGB', (1, 1))
        self.assertIsNone(functions.save_to_clipboard(test_image), "Should be None for functions.save_to_clipboard")

    def test_show_notification(self):
        text = "Test notification, also add me ElainaIsMyWaifu (Minecraft)"
        self.assertIsNone(functions.show_notification(text), "Should be None for functions.show_notification")

    def test_upload_to_discord(self):
        if no_discord:
            return
        cfgp = CfgParser()
        config = cfgp.config
        webhook = config["save_locations"]["discord"]["webhook_uri"]
        if webhook.strip() == "":
            raise RuntimeError("Please add a Webhook to config.json to do upload_to_discord test.")
        text = "Discord bad Matrix good"
        test_image = Image.new('RGB', (1, 1))
        self.assertIsNone(functions.upload_to_discord(webhook, text, test_image), "Should be None for functions.upload_to_discord")

def main():
    parser = argparse.ArgumentParser(prog="test.py", description="Test PyZoomCapturer functions.py")
    parser.add_argument('-d', '--discord', help="Don't do Discord unit test.", action="store_true")
    args, _ = parser.parse_known_args()
    if args.discord:
        global no_discord
        no_discord = True
        print("Discord unit test disabled.")
    unittest.main()

if __name__ == '__main__':
    main()
    