#!/bin/bash

export WINEPREFIX=$(mktemp -d)
echo "Current prefix: $WINEPREFIX"
echo "Downloading UPx..."
aria2c https://github.com/upx/upx/releases/download/v3.96/upx-3.96-win64.zip
7z -y x upx-3.96-win64.zip
echo "Installing Python for Windows..."
aria2c https://www.python.org/ftp/python/3.9.5/python-3.9.5-amd64.exe
xvfb-run winecfg /v win10 # Python 3.9 requires Windows 8.1 or later
xvfb-run wine ./python-3.9.5-amd64.exe /quiet InstallAllUsers=1 PrependPath=1 Include_test=0
echo "Building app..."
xvfb-run wine py -m pip install -r ./src/win_req.txt
xvfb-run wine py -m pip install pyinstaller 
xvfb-run wine py -m PyInstaller --upx-dir ./upx-*/ ./src/app.spec
echo "Building completed"
